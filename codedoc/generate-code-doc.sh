#!/bin/bash

### go to this directory
cd $(dirname $0)

### generate HTML and PDF documentation using phpDocumentor
rm -rf phpwebapp-phpdocu/
./phpdoc_html.sh
./phpdoc_pdf.sh

### generate HTML and PDF documentation using doxygen
rm -rf phpwebapp-doxygen
/usr/local/bin/doxygen doxygen.cfg
cd phpwebapp-doxygen/latex/
pdflatex refman
cd ../..

### create phpDocumentor downloadable files
rm -rf get/
mkdir get
tar cfz get/phpwebapp-phpdocu.tar.gz phpwebapp-phpdocu/
cp get/documentation.pdf get/phpwebapp-phpdocu.pdf
gzip get/phpwebapp-phpdocu.pdf
mv get/documentation.pdf get/phpwebapp-phpdocu.pdf

### create doxygen downloadable files
tar cfz get/phpwebapp-doxygen.tar.gz phpwebapp-doxygen/html/
cp phpwebapp-doxygen/latex/refman.pdf get/phpwebapp-doxygen.pdf
gzip get/phpwebapp-doxygen.pdf
mv phpwebapp-doxygen/latex/refman.pdf get/phpwebapp-doxygen.pdf
