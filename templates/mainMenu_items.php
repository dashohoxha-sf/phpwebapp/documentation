<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items of the mainMenu. 
 */
$menu_items = array(
                    "about"     => "About",
                    "tutorial"  => "Tutorial",
                    "old_user_manual"   => "Old User Manual",
                    "user_manual"   => "New User Manual",
                    "webobjects"    => "Web Objects",
                    "developer_docs"=> "Developer Docs"
                    );
?>