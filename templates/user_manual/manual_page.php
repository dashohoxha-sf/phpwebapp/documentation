<?php
/* This file is part of phpWebApp. */

class manual_page extends WebObject
{
  function onRender()
    {
      $page = WebApp::getSVar("tabs3::manual->selected_item");
      $file = TPL."user_manual/manual_pages/".$page.".txt";
      WebApp::setSVar("displaytxt->file", $file);
    }
}
?>