<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items 
 * of the tabs3::manual web object. 
 */
$menu_items = array(
                    "intro"     => "Introduction",
                    "templates" => "Templates",
                    "transitions"   => "Transitions",
                    "webclass"  => "WebClass-es",
                    "webox"     => "WebBox-es",
                    "events"    => "Events",
                    "variables" => "Variables",
                    "session"   => "Session",
                    "database"  => "Database",
                    "recordset" => "Recordsets",
                    "new_app"   => "Application Structure",
                    "misc"      => "Misc"
                    );
?>