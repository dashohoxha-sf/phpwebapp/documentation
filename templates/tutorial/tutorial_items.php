<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items 
 * of the tabs2::tutorial web object. 
 */
$menu_items = array(
                    "intro"     => "0 - Introduction",
                    "transitions"   => "1 - Transitions",
                    "templates" => "2 - Templates",
                    "weboxes"   => "3 - WebBox-es",
                    "events"    => "4 - Events",
                    "sess_var"  => "5 - Session and Variables",
                    "database"  => "6 - Database",
                    "advanced"  => "7 - Advanced",
                    "documentation" => "8 - Documentation"
                    );
?>
