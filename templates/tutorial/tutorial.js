//-*- mode: C; -*-//tells emacs to use mode C for this file
/* This file is part of phpWebApp. */

/** This function opens a new window for displaying a code file. */
function codeReview(file_path, highlights, lines)
{
  if (highlights==null) highlights = '';
  if (lines==null)      lines = '';
 
  var url = "codeReview.php?file="+file_path
    +"&highlights="+highlights
    +"&lines="+lines; 
  var features = "menubar=no,toolbar=no,status=no,"
    +"location=no,resizable=yes,scrollbars=yes";

  window.open(url, "codeReview", features);
} 

