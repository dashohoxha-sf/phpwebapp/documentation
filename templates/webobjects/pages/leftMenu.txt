
WebObject 'leftMenu' displays a menu, usually on the left side of the page.
It is used like this:

    <Include SRC="{{#LEFTMENU_PATH}}leftMenu.html" />

    <WebObject Class="leftMenu" Name="su"
    items="{{#./}}su_menu_items.php" />

The attribute 'items' is a PHP file that contains the items of the menu,
like this:
    <?
    /** 
     * The $menu_items array contains the items of the leftMenu. 
     */
    $menu_items = array(
        "item1" => " Menu Item 1 ",
        "item2" => " Menu Item 2 ",
        "item3" => " Menu Item 3 ",
        "etc1"  => " . . . ",
        "etc2"  => " . . . "
            );
    ?>

The item of the menu that is selected can be retrieved like this:
    $selected = WebApp::getSVar("leftMenu::su->selected_item");

If you need to change the look of the leftMenu, then make a local
copy and modify 'leftMenu.css'.
