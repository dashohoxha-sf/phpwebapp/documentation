<?php
/* This file is part of phpWebApp. */

class wobj_page extends WebObject
{
  function onRender()
    {
      $webobj = WebApp::getSVar("tabs2::webobj->selected_item");
      $file = TPL."webobjects/pages/".$webobj.".txt";
      WebApp::setSVar("displaytxt->file", $file);
    }
}
?>