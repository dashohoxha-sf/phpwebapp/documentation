<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items 
 * of the tabs2::tutorial web object. 
 */
$menu_items = array(
                    "tabs"     => "tabs",
                    "browse"   => "fileBrowser",
                    "folderListing" => "folderListing",
                    "form"     => "form",
                    "datebox"  => "datebox",
                    "table"    => "table",
                    "dbTable"  => "dbTable",
                    "listbox"  => "listbox",
                    "xlistbox" => "xlistbox"
                    );
?>