<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/


class content extends WebObject
{
  function init()
    {
      $this->init_docbook();
    }

  /** initialize docbook and webnotes variables */
  function init_docbook()
    {
      $cache_path = BOOKS_PATH.'content/books/cache/';
      WebApp::setSVar('docbook->cache_path', $cache_path);
      WebApp::setSVar('docbook->book_id', 'phpwebapp_manual');
      WebApp::setSVar('docbook->webnotes', 'true');

      //set the parameters of the webnotes
      WebApp::setSVar('webnotes->visible', 'false');
      WebApp::setSVar('webnotes->page_id', '');
      WebApp::setSVar('webnotes->notify', 'true');
      WebApp::setSVar('webnotes->emails', 'dashohoxha@users.sourceforge.net');
      WebApp::setSVar('webnotes->unmoderated', 'true');
      WebApp::setSVar('webnotes->approve', 'false');
      WebApp::setSVar('webnotes->admin', 'false');

      //set the database parameters
      WebApp::setSVar('webnotes->dbhost', 'localhost');
      WebApp::setSVar('webnotes->dbuser', 'root');
      WebApp::setSVar('webnotes->dbpasswd', '');
      WebApp::setSVar('webnotes->dbname', 'webnotes');
    }

  function onParse()
    {
      //set the {{content_file}} according to the selected tab
      $tab = WebApp::getSVar("tabs1::mainMenu->selected_item");
      switch ($tab)
        {
        default:
        case "about":
          $content_file = "about.html";
          break;
        case "tutorial":
          $content_file = "tutorial/tutorial.html";
          break;
        case "old_user_manual":
          $content_file = "user_manual/user_manual.html";
          break;
        case "user_manual":
          $content_file = "user_manual.html";
          break;
        case "developer_docs":
          $content_file = "developer_docs.html";
          break;
        case "webobjects":
          $content_file = "webobjects/webobjects.html";
          break;
        }
      WebApp::addVar("content_file", $content_file);
    }
}
?>